<?php

namespace Morris\PORM;

use \PDO;
use \PDOStatement;

/**
 * Class DB
 * @package Morris\PORM
 */
class DB
{
    /**
     * instance tree
     * @var
     */
    private static $_instance_tree = [];
    /**
     * current instance name
     * @var
     */
    private static $_instance_name = '';
    /**
     * connect instance
     * @var PDO
     */
    private $_conn = null;

    /**
     * error message
     * @var string
     */
    private $_error = '';

    /**
     * errno
     * @var int
     */
    private $_errno = 0;
    /**
     * transaction num
     * @var int
     */
    private $_transaction_num = 0;
    /**
     * config tree
     * @var array
     */
    private static $_config_tree = [];

    /**
     * $class@$method
     * @var
     */
    private static $_log_handle = null;

    /**
     * $class@$method
     * @var string
     */
    private static $_sql_error_handle = '';

    /**
     * @return DB
     */
    public static function getInstance()
    {
        // get instance
        if (empty(self::$_instance_tree[self::$_instance_name]) || !(self::$_instance_tree[self::$_instance_name] instanceof DB)) {
            self::$_instance_tree[self::$_instance_name] = new DB;
        }
        return self::$_instance_tree[self::$_instance_name];
    }

    /**
     * constructor.
     */
    public function __construct()
    {
        $config = self::$_config_tree[self::$_instance_name] ?? [];
        if (empty($config)) {
            die('config empty');
        }
        $host = $config['host'] ?? '';
        $port = $config['port'] ?? 3306;
        $dbname = $config['dbname'] ?? '';
        $user = $config['user'] ?? '';
        $password = $config['password'] ?? '';
        $dns = "mysql:dbname={$dbname};host={$host}:{$port}";
        if ($this->_conn == null) {
            $this->_conn = new PDO($dns, $user, $password);
            if (empty($this->_conn)) {
                $this->_setError("db connect fail");
            }
            $this->_conn->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
            $this->_conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        }
    }

    /**
     * destruct.
     */
    public function __destruct()
    {
        $this->close();
    }

    /**
     * @param $config
     */
    public static function setConfig($config)
    {
        if (empty($config['host'])
            || empty($config['port'])
            || empty($config['user'])
            || empty($config['password'])
            || empty($config['dbname'])
        ) {
            die("config error");
        }
        $instance_name = $config['host'] . $config['port'] . $config['dbname'];
        if (!isset(self::$_config_tree[$instance_name])) {
            self::$_config_tree[$instance_name] = $config;
            self::$_instance_name = $instance_name;
        }
    }

    /**
     * set log handle
     * @param Closure $callback
     */
    public static function setLogHandle(\Closure $callback)
    {
        self::$_log_handle = $callback;
    }

    /**
     * find single record.
     *
     * @param string $sql
     * @param array $params
     * @return bool|mixed
     */
    public function find($sql, $params)
    {
        $statement = $this->_getStatement($sql);
        if (false == $statement) {
            return false;
        }
        $statement->execute($params);
        if (!empty($statement->errorCode()) && $statement->errorCode() != '00000') {
            return $this->_setError($statement->errorInfo());
        }
        $res = $statement->fetch(PDO::FETCH_ASSOC);
        return $res;
    }

    /**
     * get record
     * @param string $sql
     * @param array $params
     * @return array|bool
     */
    public function get(string $sql, array $params): array
    {
        $statement = $this->_getStatement($sql);
        if (false == $statement) {
            return [];
        }
        $res = $statement->execute($params);
        if (false == $res) {
            $this->_setError($statement->errorInfo());
            return [];
        }
        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param $sql
     * @param $params
     * @return bool
     */
    public function exec($sql, $params)
    {
        $statement = $this->_getStatement($sql);
        if (false == $statement) {
            return false;
        }
        $res = $statement->execute($params);
        if (false == $res) {
            return $this->_setError($statement->errorInfo());
        }
        return true;
    }

    /**
     * get last insert id.
     * @return string
     */
    public function getLastInsertId()
    {
        return $this->_conn->lastInsertId();
    }

    /**
     * 获取数据总数
     *
     * @return
     */
    public function count()
    {
        $sql = "SELECT FOUND_ROWS() AS count";
        $res = DB::getInstance()->find($sql, []);
        return $res['count'] ?? 0;
    }

    /**
     * begin transaction
     */
    public function beginTransaction()
    {
        if ($this->_transaction_num == 1) {
            return ;
        }
        $this->_conn->beginTransaction();
        $this->_transaction_num = 1;
    }

    /**
     * commit transaction
     */
    public function commit()
    {
        if ($this->_transaction_num == 0) {
            return;
        }
        $this->_conn->commit();
        $this->_transaction_num = 0;
    }

    /**
     * rollback transaction
     */
    public function rollback()
    {
        if ($this->_transaction_num == 0) {
            return;
        }
        $this->_conn->rollback();
        $this->_transaction_num = 0;
    }

    /**
     * get error
     * @return string
     */
    public function getError()
    {
        return $this->_error;
    }

    /**
     * get errno
     * @return int
     */
    public function getErrno()
    {
        return $this->_errno;
    }

    /**
     * getSql
     *
     * @param string $sql
     * @param array $param
     * @return string
     */
    public function getSql($sql, $param = [])
    {
        if (empty($param)) {
            return $sql;
        }
        return str_replace(array_keys($param), array_values($param), $sql);
    }

    /**
     * close connect
     */
    public function close()
    {
//        $this->_conn->c
    }

    public function logSql($sql, $params)
    {
        if (!empty(self::$_log_handle)) {
            $sql = DB::getInstance()->getSql($sql, $params);
            call_user_func_array(self::$_log_handle, ['success', $sql]);
        }
    }

    /**
     * log error sql
     * @param $sql
     * @param $params
     */
    public function logErrorSql($sql, $params)
    {
        if (!empty(self::$_log_handle)) {
            $sql = DB::getInstance()->getSql($sql, $params);
            call_user_func_array(self::$_log_handle, ['fail',  'ERROR: ' . $this->getError() . '; ERRNO: ' . $this->getErrno() . '; Sql: ' . $sql]);
        }
    }

    /**
     * get pdo prepare statement
     * @param $sql
     * @return PDOStatement | bool
     */
    private function _getStatement($sql)
    {
        $res = $this->_conn->prepare($sql);
        if (empty($res)) {
            return $this->_setError($this->_conn->errorInfo());
        }
        return $res;
    }

    /**
     * set error
     * @param $error
     * @return bool
     */
    private function _setError($error)
    {
        list($errno, $_ignore, $error) = $error;
        $this->_error = $error;
        $this->_errno = (int)$errno;
        return false;
    }
}