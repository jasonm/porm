<?php

namespace Morris\PORM;

/**
 * Class Model
 * @package Morris\PORM
 */
class Model
{
    /**
     * table name
     * @var string
     */
    protected $_table = '';

    /**
     * @var bool
     */
    protected $_soft_delete = false;

    /**
     * primary key
     * @var string
     */
    protected $_primary_key = 'id';

    /**
     * create time
     * @var string
     */
    protected $_created_at = 'created_at';

    /**
     * update time
     * @var string
     */
    protected $_updated_at = 'updated_at';

    /**
     * soft delete time
     * @var string
     */
    protected $_delete_at = 'deleted_at';

    /**
     * where conditions
     * @var array
     */
    private $_where = [];

    /**
     * query with trash
     * @var bool
     */
    private $_with_trash = false;

    /**
     * limit
     * @var string
     */
    private $_limit = '';

    /**
     * group
     * @var string
     */
    private $_group = '';

    /**
     * order
     * @var string
     */
    private $_order = '';

    /**
     * join
     * @var array
     */
    private $_join = [];

    /**
     * model tree
     * @var array
     */
    private static $_model_tree = [];

    /**
     * query fields
     * @var string
     */
    private $_fields = '';

    /**
     * error
     * @var string
     */
    private $_error = '';

    /**
     * get instance
     * @param string $clz
     * @return Model
     */
    public static function run($clz)
    {
        if (!isset(self::$_model_tree[$clz])) {
            self::$_model_tree[$clz] = new $clz();
        } else {
            // if isset, clear.
            (self::$_model_tree[$clz])->clear();
        }
        return self::$_model_tree[$clz];
    }

    /**
     * get table
     * @return string
     */
    public function getTable(): string
    {
        return $this->_table;
    }

    /**
     * @param string $field
     * @param $condition
     * @param string $value
     * @return $this
     */
    public function where(string $field, $condition, $value = 'nil')
    {
        if (!isset($this->_where[$field])) {
            $this->_where[$field] = [];
        }
        if ($value === 'nil') {
            $value = $condition;
            $condition = '=';
        }
        array_push($this->_where[$field], [$condition, $value]);
        return $this;
    }

    /**
     * @param string $field
     * @param array $value
     * @return $this
     */
    public function whereIn(string $field, array $value)
    {
        if (!isset($this->_where[$field])) {
            $this->_where[$field] = [];
        }
        array_push($this->_where[$field], ['IN', $value]);
        return $this;
    }

    /**
     * @param string $field
     * @param array $value
     * @return $this
     */
    public function whereNotIn(string $field, array $value)
    {
        if (!isset($this->_where[$field])) {
            $this->_where[$field] = [];
        }
        array_push($this->_where[$field], ['NOT IN', $value]);
        return $this;
    }

    /**
     * @return $this
     */
    public function withTrash()
    {
        $this->_with_trash = true;
        return $this;
    }

    /**
     * join
     * @param string $table
     * @param $on
     * @return $this
     */
    public function join(string $table, $on)
    {
        array_push($this->_join, [
            'op' => 'JOIN',
            'table' => $table,
            'on' => $on
        ]);
        return $this;
    }

    /**
     * left join
     * @param string $table
     * @param $on
     * @return $this
     */
    public function leftJoin(string $table, $on)
    {
        array_push($this->_join, [
            'op' => 'LEFT JOIN',
            'table' => $table,
            'on' => $on
        ]);
        return $this;
    }

    /**
     * right join
     * @param string $table
     * @param $on
     * @return $this
     */
    public function rightJoin(string $table, $on)
    {
        array_push($this->_join, [
            'op' => 'RIGHT JOIN',
            'table' => $table,
            'on' => $on
        ]);
        return $this;
    }

    /**
     * set limit
     * @param string $limit
     * @return $this
     */
    public function limit(string $limit)
    {
        $this->_limit = $limit;
        return $this;
    }

    /**
     * set group by
     * @param string $group_by
     * @return $this
     */
    public function groupBy(string $group_by)
    {
        $this->_group = $group_by;
        return $this;
    }

    /**
     * @param string $order_by
     * @return $this
     */
    public function orderBy(string $order_by)
    {
        $this->_order = $order_by;
        return $this;
    }

    /**
     * select
     *
     * @param $sql
     * @param array $params
     * @return array|bool
     */
    public function query($sql, $params = [])
    {
        $res = DB::getInstance()->get($sql, $params);
        if (DB::getInstance()->getErrno() != 0) {
            $this->_error = DB::getInstance()->getError();
        }
        return $res;
    }

    /**
     * exec: insert update insert
     *
     * @param $sql
     * @param array $params
     * @return bool
     */
    public function exec($sql, $params = [])
    {
        $res = DB::getInstance()->exec($sql, $params);
        if (DB::getInstance()->getErrno() != 0) {
            $this->_error = DB::getInstance()->getError();
        }
        return $res;
    }

    /**
     * find
     *
     * @param array $fields
     * @return array|bool|mixed
     */
    public function find(array $fields = ['*'])
    {
        $field_str = '*';
        if ($fields != ['*'] && is_array($fields)) {
            $field_str = implode(',', $fields);
        }
        $this->_fields = $field_str;
        list($where, $params) = $this->_parseWhere();
        $limit = ' LIMIT 1';
        $group = $this->_parseGroup();
        $order = $this->_parseOrder();
        $join = $this->_parseJoin();
        $sql = "SELECT {$field_str} FROM {$this->_table}{$join}{$where}{$group}{$order}{$limit}";
        $res = DB::getInstance()->find($sql, $params);
        if (DB::getInstance()->getErrno() != 0) {
            $this->_error = DB::getInstance()->getError();
            DB::getInstance()->logErrorSql($sql, $params);
            return false;
        } else {
            DB::getInstance()->logSql($sql, $params);
        }
        return $res;
    }

    /**
     * get
     *
     * @param array $fields
     * @return array|bool|mixed
     */
    public function get(array $fields = ['*'])
    {
        $field_str = '*';
        if ($fields != ['*'] && is_array($fields)) {
            $field_str = implode(',', $fields);
        }
        $this->_fields = $field_str;
        list($where, $params) = $this->_parseWhere();
        $limit = $this->_parseLimit();
        $group = $this->_parseGroup();
        $order = $this->_parseOrder();
        $join = $this->_parseJoin();
        $sql = "SELECT {$field_str} FROM {$this->_table}{$join}{$where}{$group}{$order}{$limit}";
        $res = DB::getInstance()->get($sql, $params);
        if (DB::getInstance()->getErrno() != 0) {
            $this->_error = DB::getInstance()->getError();
            DB::getInstance()->logErrorSql($sql, $params);
            return [];
        } else {
            DB::getInstance()->logSql($sql, $params);
        }
        return $res;
    }

    /**
     * @return
     */
    public function count()
    {
        return DB::getInstance()->count();
    }

    /**
     * @param array $data
     * @return bool|string
     */
    public function insert(array $data)
    {
        if (empty($data)) {
            $this->_error = 'Insert data empty!';
            return false;
        }
        // created_at updated_at
        if (empty($data[$this->_created_at])) {
            $data[$this->_created_at] = time();
        }
        if (empty($data[$this->_updated_at])) {
            $data[$this->_updated_at] = time();
        }
        $fields = '';
        $params = [];
        $value_str = '';
        foreach ($data as $field => $value) {
            $params[":{$field}"] = $value;
            $fields .= "`{$field}`,";
            $value_str .= ":{$field},";
        }
        $fields = substr($fields, 0, strlen($fields) - 1);
        $value_str = substr($value_str, 0, strlen($value_str) - 1);
        $sql = "INSERT INTO {$this->_table} ({$fields}) VALUES ({$value_str})";
        DB::getInstance()->exec($sql, $params);
        if (DB::getInstance()->getErrno() != 0) {
            $this->_error = DB::getInstance()->getError();
            DB::getInstance()->logErrorSql($sql, $params);
            return false;
        } else {
            DB::getInstance()->logSql($sql, $params);
        }
        return DB::getInstance()->getLastInsertId();
    }

    /**
     * @param $data
     * @return bool
     */
    public function insertMulti($data)
    {
        if (empty($data)) {
            $this->_error = 'Insert data empty!';
            return false;
        }
        $fields = '';
        $params = [];
        $created_at = time();
        $updated_at = time();
        $value_arr = [];
        foreach ($data as $index => $temp) {
            $value_str = '';
            // created_at updated_at
            if (empty($temp[$this->_created_at])) {
                $temp[$this->_created_at] = $created_at;
            }
            if (empty($temp[$this->_updated_at])) {
                $temp[$this->_updated_at] = $updated_at;
            }
            foreach ($temp as $field => $value) {
                if ($index == 0) {
                    $fields .= "`{$field}`,";
                }
                $params[":{$field}_{$index}"] = $value;
                $value_str .= ":{$field}_{$index},";
            }
            $value_str = substr($value_str, 0, strlen($value_str) - 1);
            array_push($value_arr, "({$value_str})");
        }
        $fields = substr($fields, 0, strlen($fields) - 1);
        $value_str = implode(',', $value_arr);
        $sql = "INSERT INTO {$this->_table} ({$fields}) VALUES {$value_str}";
        DB::getInstance()->exec($sql, $params);
        if (DB::getInstance()->getErrno() != 0) {
            $this->_error = DB::getInstance()->getError();
            DB::getInstance()->logErrorSql($sql, $params);
            return false;
        } else {
            DB::getInstance()->logSql($sql, $params);
        }
        return true;
    }

    /**
     * update
     * @param $data
     * @return bool
     */
    public function update($data)
    {
        if (empty($data)) {
            $this->_error = 'Update data empty';
            return false;
        }
        $data[$this->_updated_at] = time();
        $update_str = 'SET ';
        foreach ($data as $field => $value) {
            if (is_string($value)) {
                $value = "'{$value}'";
            }
            $update_str .= "`{$field}`={$value},";
        }
        $update_str = substr($update_str, 0, strlen($update_str) - 1);
        list($where, $params) = $this->_parseWhere();
        $limit = $this->_parseLimit();
        $group = $this->_parseGroup();
        $sql = "UPDATE {$this->_table} {$update_str} {$where} {$limit} {$group}";
        $res = DB::getInstance()->exec($sql, $params);
        if (DB::getInstance()->getErrno() != 0) {
            $this->_error = DB::getInstance()->getError();
            DB::getInstance()->logErrorSql($sql, $params);
            return false;
        } else {
            DB::getInstance()->logSql($sql, $params);
        }
        return $res;
    }

    /**
     * save: insert or update
     *
     * @param $data
     * @return int
     */
    public function save($data): int
    {
        // data empty
        if (empty($data)) {
            $this->_error = 'Save data empty!';
            return false;
        }
        if (isset($data[$this->_primary_key])) {
            // update
            $res = $data[$this->_primary_key];
            $this->where($this->_primary_key, $res);
            unset($data[$this->_primary_key]);
            if (false == $this->update($data)) {
                $res = 0;
            }
        } else {
            //insert
            $res = $this->insert($data);
        }
        return $res;
    }

    /**
     * soft delete or delete.
     * @return bool
     */
    public function delete()
    {
        if (empty($this->_where)) {
            $this->_error = 'Delete condition empty!';
            return false;
        }
        if (!empty($this->_delete_at)) {
            // soft delete
            $data = [$this->_delete_at => time()];
            return $this->update($data);
        } else {
            return $this->destroy();
        }
    }

    /**
     * delete
     * @return bool
     */
    public function destroy()
    {
        if (empty($this->_where)) {
            $this->_error = 'Destroy condition empty!';
            return false;
        }
        list($where_str, $params) = $this->_parseWhere();
        $sql = "DELETE FROM {$this->_table} {$where_str}";
        DB::getInstance()->exec($sql, $params);
        if (DB::getInstance()->getErrno() != 0) {
            $this->_error = DB::getInstance()->getError();
            DB::getInstance()->logErrorSql($sql, $params);
            return false;
        } else {
            DB::getInstance()->logSql($sql, $params);
        }
        return true;
    }

    /**
     * beginTransaction
     */
    public function beginTransaction()
    {
        Db::getInstance()->beginTransaction();
    }

    /**
     * commit
     */
    public function commit()
    {
        Db::getInstance()->commit();
    }

    /**
     * rollback
     */
    public function rollback()
    {
        Db::getInstance()->rollback();
    }

    /**
     * get error
     * @return string
     */
    public function getError()
    {
        return $this->_error;
    }

    /**
     * clear
     */
    public function clear()
    {
        if (!empty($this->_where)) {
            $this->_where = [];
        }
        if ($this->_with_trash == true) {
            $this->_with_trash = false;
        }
        if (!empty($this->_limit)) {
            $this->_limit = '';
        }
        if (!empty($this->_group)) {
            $this->_group = '';
        }
        if (!empty($this->_order)) {
            $this->_order = '';
        }
        if (!empty($this->_join)) {
            $this->_join = [];
        }
    }

    /**
     * parse where
     * @return array
     */
    private function _parseWhere()
    {
        // soft delete
        if ($this->_soft_delete == true && $this->_with_trash == false) {
            if (!isset($this->_where[$this->_delete_at])) {
                $this->_where[$this->_delete_at] = [];
            }
            array_push($this->_where[$this->_delete_at], ['=', 0]);
        }
        if (empty($this->_where)) {
            return ['', []];
        }
        $where = [];
        $params = [];
        foreach ($this->_where as $field => $map) {
            $index = 0;
            foreach ($map as $temp) {
                // condition: in or not in
                if (in_array($temp[0], ['IN', 'NOT IN'])) {
                    if (empty($temp[1])) {
                        continue;
                    }
                    $where_in_or_not_in = [];
                    foreach ($temp[1] as $t_index => $tv) {
                        array_push($where_in_or_not_in, ":{$field}_in_{$index}_$t_index");
                        $params[":{$field}_in_{$index}_$t_index"] = $tv;
                    }
                    $where_in_or_not_in_str = implode(',', $where_in_or_not_in);
                    array_push($where, "`{$field}` {$temp[0]} ({$where_in_or_not_in_str})");
                } else {
                    array_push($where, "`{$field}` {$temp[0]} :{$field}_{$index}");
                    $params[":{$field}_{$index}"] = $temp[1];
                }
                $index++;
            }
        }
        $where_str = ' WHERE ' . implode(' AND ', $where);
        return [$where_str, $params];
    }

    /**
     * parse limit
     * @return string
     */
    private function _parseLimit()
    {
        if (!empty($this->_limit)) {
            return " LIMIT {$this->_limit}";
        }
        return '';
    }

    /**
     * parse group
     * @return string
     */
    private function _parseGroup()
    {
        if (!empty($this->_group)) {
            return " GROUP BY {$this->_group}";
        }
        return '';
    }

    /**
     * parse order
     * @return string
     */
    private function _parseOrder()
    {
        if (!empty($this->_order)) {
            return " ORDER BY {$this->_order}";
        }
        return '';
    }

    /**
     * parse join
     * @return string
     */
    private function _parseJoin()
    {
        if (empty($this->_join)) {
            return '';
        }
        $str = '';
        foreach ($this->_join as $temp) {
            $str .= " {$temp['op']} {$temp['table']} ON {$temp['on']}";
        }
        return $str . ' ';
    }
}
